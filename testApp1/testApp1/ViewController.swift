//
//  ViewController.swift
//  testApp1
//
//  Created by Denis Biškup on 16.08.2016..
//  Copyright © 2016. Denis Biškup. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var poljeA: UITextField!
    @IBOutlet weak var poljeB: UITextField!
    @IBOutlet weak var poljeC: UITextField!
    @IBOutlet weak var rez: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        poljeA.keyboardType = .DecimalPad
        poljeB.keyboardType = .DecimalPad
        poljeC.keyboardType = .DecimalPad
        
        poljeA.becomeFirstResponder()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func calc(sender: AnyObject) {
        if let poljeAText = poljeA.text,
            poljeBText = poljeB.text,
            poljeCText = poljeC.text {
            
            if !poljeAText.isEmpty && !poljeBText.isEmpty && !poljeCText.isEmpty {
                if let vrijednostA = Double(poljeAText), let vrijednostB = Double(poljeBText), let vrijednostC = Double(poljeCText) {
                    let zbroj = vrijednostA + vrijednostB + vrijednostC
                    rez.text = String(format: "%.2lf", zbroj)
                }
                
            } else {
                var praznaPolja = Array<String>()
                let poruka="bla"
            
                if poljeAText.isEmpty {
                    praznaPolja.append("A")
                }
                if poljeBText.isEmpty {
                    praznaPolja.append("B")
                }
                if poljeCText.isEmpty {
                    praznaPolja.append("C")
                }
            
                let stringRepresentation = praznaPolja.joinWithSeparator(", ")
            
                let alertController = UIAlertController(title: "Upozorenje!", message: String(format: "%@ %@!", poruka, stringRepresentation), preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "U redu", style: .Destructive, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
        
    }
}

